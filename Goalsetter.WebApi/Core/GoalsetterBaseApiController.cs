﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rental.Services.Enums;
using Rental.Services.Extensions;
using System;

namespace Goalsetter.Core
{
    public class GoalsetterBaseApiController : ControllerBase
    {

        public ILogger Logger { get; set; }
        public IMapper Mapper { get; set; }
        public GoalsetterBaseApiController(IMapper mapper, ILoggerFactory logFactory)
        {
            Mapper = mapper;
            Logger = logFactory.CreateLogger<GoalsetterBaseApiController>();

        }
        protected ObjectResult BuildExceptionResult(Exception ex)
        {
            var problemDetails = new ValidationProblemDetails();
            while (ex != null)
            {
                problemDetails.Errors.Add(ex.Source.ToString(), new string[] { ex.Message });
                ex = ex.InnerException;
            }
            return new ObjectResult(problemDetails)
            {
                ContentTypes = { "application/problem+json" },
                StatusCode = 400,
            };
        }
        protected ObjectResult BuildErrorResult(string ServerErrorMessage)
        {
            var problemDetails = new ValidationProblemDetails(ModelState);
            problemDetails.Errors.Add("errors", new string[] { ServerErrorMessage });
            return new ObjectResult(problemDetails)
            {
                ContentTypes = { "application/problem+json" },
                StatusCode = 400,
            };
        }
        protected ObjectResult BuildResult<T>(ReturnStatus returnStatus)
        {
            string entity = $"{typeof(T).Name}";
            if (returnStatus != ReturnStatus.Success)
            {
                var problemDetails = new ValidationProblemDetails(ModelState);
                problemDetails.Errors.Add(entity, new string[] { returnStatus.GetDescription() });
                return new ObjectResult(problemDetails)
                {
                    ContentTypes = { "application/problem+json" },
                    StatusCode = 400,
                };
            }
            return Ok(returnStatus.GetDescription());
        }
    }
}
