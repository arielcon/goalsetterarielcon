﻿namespace Goalsetter.Helpers
{
    public class AppSettings
    {
        public string Key { get; set; }

        public string InitTestData { get; set; }
    }
}
