﻿using AutoMapper;
using Goalsetter.Core;
using Goalsetter.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rental.Data.Models;
using Rental.Presentation.DTOs;
using Rental.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Goalsetter.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : GoalsetterBaseApiController
    {
        private readonly IVehicleService _vehicleService;
        public VehiclesController(IVehicleService vehicleService, IMapper Mapper, ILoggerFactory logFactory) : base(Mapper, logFactory)
        {
            _vehicleService = vehicleService;
        }

        // GET: api/Vehicles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleDto>>> GetVehicles()
        {
            var vehicles = await _vehicleService.GetAllAsync();
            var vehiclesResult = Mapper.Map<IEnumerable<Vehicle>, IEnumerable<VehicleDto>>(vehicles);
            return vehiclesResult.ToList();
        }

        // GET: api/Vehicles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VehicleDto>> GetVehicle(int id)
        {
            var vehicle = await _vehicleService.GetByIdAsync(id);
            if (vehicle == null)
            {
                return BuildErrorResult("Vehicle Not Found");
            }
            var vehicleResult = Mapper.Map<VehicleDto>(vehicle);
            return vehicleResult;
        }

        // PUT: api/Vehicles/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [ValidateModelState]
        public async Task<IActionResult> PutVehicle(int id, [FromBody] VehicleDto vehicleDTO)
        {
            if (id != vehicleDTO.Id)
            {
                return BuildErrorResult("Invalid Vehicle information");
            }
            try
            {
                var vehicle = Mapper.Map<Vehicle>(vehicleDTO);
                var result = await _vehicleService.UpdateAsync(vehicle);
                return BuildResult<VehicleDto>(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return BuildExceptionResult(ex);
            }
        }

        // POST: api/Vehicles
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [ValidateModelState]
        public async Task<ActionResult<VehicleDto>> PostVehicle([FromBody] VehicleDto vehicleDTO)
        {
            try
            {
                var vehicle = Mapper.Map<Vehicle>(vehicleDTO);
                var result = await _vehicleService.AddAsync(vehicle);
                return BuildResult<VehicleDto>(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return BuildExceptionResult(ex);
            }
        }

        // DELETE: api/Vehicles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<VehicleDto>> DeleteVehicle(int id)
        {
            try
            {
                var result = await _vehicleService.DeleteAsync(id);
                return BuildResult<VehicleDto>(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return BuildExceptionResult(ex);
            }
        }
    }
}
