﻿using AutoMapper;
using Goalsetter.Core;
using Goalsetter.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rental.Data.Models;
using Rental.Presentation.DTOs;
using Rental.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Goalsetter.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RentalsController : GoalsetterBaseApiController
    {
        private readonly IRentalService _rentalService;
        public RentalsController(IRentalService RentalService, IMapper Mapper, ILoggerFactory logFactory) : base(Mapper, logFactory)
        {
            _rentalService = RentalService;
        }

        // GET: api/Rentals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RentalEntryDto>>> GetRentals()
        {
            var rentals = await _rentalService.GetAllAsync();
            var rentalsResult = Mapper.Map<IEnumerable<RentalEntry>, IEnumerable<RentalEntryDto>>(rentals);
            return rentalsResult.ToList();
        }

        // GET: api/Rentals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RentalEntryDto>> GetRental(int id)
        {
            var rental = await _rentalService.GetByIdAsync(id);
            if (rental == null)
            {
                return BuildErrorResult("Rental Not Found");
            }
            var rentalResult = Mapper.Map<RentalEntryDto>(rental);
            return rentalResult;
        }

        // POST: api/Rentals
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [ValidateModelState]
        public async Task<ActionResult<RentalEntryDto>> PostRental([FromBody] RentalEntryDto rentalDTO)
        {
            try
            {
                var rental = Mapper.Map<RentalEntry>(rentalDTO);
                var result = await _rentalService.AddAsync(rental);
                return BuildResult<RentalEntryDto>(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return BuildExceptionResult(ex);
            }
        }

        // DELETE: api/Rentals/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RentalEntryDto>> CancelRental(int id)
        {
            try
            {
                var result = await _rentalService.CancelRental(id);
                return BuildResult<RentalEntryDto>(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return BuildExceptionResult(ex);
            }
        }
    }
}
