﻿using AutoMapper;
using Goalsetter.Core;
using Goalsetter.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rental.Data.Models;
using Rental.Presentation.DTOs;
using Rental.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Goalsetter.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : GoalsetterBaseApiController
    {
        private readonly IClientService _clientService;

        public ClientsController(IClientService clientService, IMapper Mapper, ILoggerFactory logFactory) : base(Mapper, logFactory)
        {
            _clientService = clientService;
        }
        // GET: api/<ClientsController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientDto>>> GetClients()
        {
            var clients = await _clientService.GetAllAsync();
            var clientResult = Mapper.Map<IEnumerable<Client>, IEnumerable<ClientDto>>(clients);
            return clientResult.ToList();
        }

        // GET api/<ClientsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ClientDto>> GetClient(int id)
        {
            var client = await _clientService.GetByIdAsync(id);
            if (client == null)
            {
                return BuildErrorResult("Client Not found");
            }
            var clientResult = Mapper.Map<ClientDto>(client);
            return clientResult;
        }

        // POST api/<ClientsController>
        [HttpPost]
        [ValidateModelState]
        public async Task<ActionResult<ClientDto>> PostClient([FromBody] ClientDto clientDTO)
        {
            try
            {
                var client = Mapper.Map<Client>(clientDTO);
                var result = await _clientService.AddAsync(client);
                return BuildResult<ClientDto>(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return BuildExceptionResult(ex);
            }
        }

        // PUT api/<ClientsController>/5
        [HttpPut("{id}")]
        [ValidateModelState]
        public async Task<ActionResult<ClientDto>> PutClient(int id, [FromBody] ClientDto clientDTO)
        {
            if (id != clientDTO.Id)
            {
                return BuildErrorResult("Invalid client information");
            }
            try
            {
                var client = Mapper.Map<Client>(clientDTO);
                var result = await _clientService.UpdateAsync(client);
                return BuildResult<ClientDto>(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return BuildExceptionResult(ex);
            }
        }

        // DELETE api/<ClientsController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ClientDto>> DeleteClient(int id)
        {
            try
            {
                var result = await _clientService.DeleteAsync(id);
                return BuildResult<ClientDto>(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return BuildExceptionResult(ex);
            }

        }
    }
}
