using AutoMapper;
using Goalsetter.Extensions;
using Goalsetter.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Rental.Data.Context;
using Rental.Data.Interfaces;
using Rental.Data.Repositories;
using Rental.Service.Services;
using Rental.Services.BusinessLogic;
using Rental.Services.Interfaces;
using Rental.TestData;
using System;

namespace Goalsetter
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //var appSettingsSection = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("AppSettings");
            //services.Configure<AppSettings>(appSettingsSection);

            string connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<GoalSetterContext>(options =>
                             options.UseSqlServer(connectionString));

            services.AddTransient<IVehiclesRepository, VehiclesRepository>();
            services.AddTransient<IBrandsRepository, BrandsRepository>();
            services.AddTransient<IRentalsRepository, RentalsRepository>();
            services.AddTransient<IClientsRepository, ClientsRepository>();
            services.AddTransient<IRentalPricesRepository, RentalPricesRepository>();

            services.AddTransient<IClientService, ClientService>();
            services.AddTransient<IVehicleService, VehicleService>();
            services.AddTransient<IBrandService, BrandService>();
            services.AddTransient<IRentalService, RentalService>();
            services.AddTransient<IReservationsValidator, ReservationsValidator>();

            services.AddAutoMapper(typeof(Startup));

            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(ValidateModelStateAttribute));
                options.AllowEmptyInputInBodyModelBinding = true;
                foreach (var formatter in options.InputFormatters)
                {
                    if (formatter.GetType() == typeof(SystemTextJsonInputFormatter))
                        ((SystemTextJsonInputFormatter)formatter).SupportedMediaTypes.Add(
                            Microsoft.Net.Http.Headers.MediaTypeHeaderValue.Parse("text/plain"));
                }

            }).AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
            });

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var initTestData = Convert.ToBoolean(AppSettings.InitTestData);
            if (initTestData)
            {
                var serviceScopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
                using (var serviceScope = serviceScopeFactory.CreateScope())
                {
                    DbInitializer.InitializeWithData(serviceScope.ServiceProvider); // Insert default data
                }
            }

        }

        private AppSettings AppSettings
        {
            get
            {
                var settings = new AppSettings();
                Configuration.GetSection("AppSettings").Bind(settings);
                return settings;
            }
        }
    }
}
