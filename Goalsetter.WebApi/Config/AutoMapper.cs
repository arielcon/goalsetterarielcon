﻿using AutoMapper;
using Rental.Data.Models;
using Rental.Presentation.DTOs;

namespace Goalsetter.Config
{
    public class AutoMapper : Profile
    {
        public AutoMapper()
        {
            CreateMap<Brand, BrandDto>();
            CreateMap<Client, ClientDto>();
            CreateMap<RentalEntry, RentalEntryDto>();
            CreateMap<RentalPrice, RentalPriceDto>();
            CreateMap<Vehicle, VehicleDto>();
            ////.ForMember("Brand", d => d.MapFrom(s => new Brand() { Active = s.Brand.Active, Id = s.Brand.Id, Name = s.Brand.Name  }));

            CreateMap<BrandDto, Brand>();
            CreateMap<ClientDto, Client>();
            CreateMap<RentalEntryDto, RentalEntry>();
            CreateMap<RentalPriceDto, RentalPrice>();
            CreateMap<VehicleDto, Vehicle>();
        }
    }
}
