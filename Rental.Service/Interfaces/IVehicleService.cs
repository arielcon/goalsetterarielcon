﻿using Rental.Data.Models;

namespace Rental.Services.Interfaces
{
    public interface IVehicleService : IService<Vehicle>
    {

    }
}