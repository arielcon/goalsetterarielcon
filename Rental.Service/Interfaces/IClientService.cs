﻿using Rental.Data.Models;

namespace Rental.Services.Interfaces
{
    public interface IClientService : IService<Client>
    {

    }
}