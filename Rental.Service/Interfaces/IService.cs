﻿using Rental.Services.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rental.Services.Interfaces
{
    public interface IService<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        Task<ReturnStatus> AddAsync(T entity);
        Task<ReturnStatus> DeleteAsync(int id);
        Task<ReturnStatus> UpdateAsync(T entity);

    }
}
