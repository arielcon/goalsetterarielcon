﻿using Rental.Data.Models;

namespace Rental.Services.Interfaces
{
    public interface IBrandService : IService<Brand>
    {

    }
}