﻿using Rental.Data.Models;
using Rental.Services.Enums;
using System.Threading.Tasks;

namespace Rental.Services.Interfaces
{
    public interface IRentalService : IService<RentalEntry>
    {
        Task<ReturnStatus> CancelRental(int id);

    }
}