﻿using Rental.Data.Interfaces;
using Rental.Data.Models;
using Rental.Services.BusinessLogic;
using Rental.Services.Enums;
using Rental.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rental.Service.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientsRepository _clientRepository;
        private readonly IReservationsValidator _reservationsValidator;
        public ClientService(IClientsRepository ClientRepository, IReservationsValidator reservationsValidator)
        {
            _clientRepository = ClientRepository;
            _reservationsValidator = reservationsValidator;
        }

        public async Task<IEnumerable<Client>> GetAllAsync()
        {
            return await _clientRepository.GetAllClientsAsync();
        }

        public async Task<Client> GetByIdAsync(int id)
        {
            var client = _clientRepository.GetClientByIdAsync(id);
            return await client;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Critical Code Smell", "S927:Parameter names should match base declaration and other partial definitions", Justification = "<Pending>")]
        public async Task<ReturnStatus> AddAsync(Client client)
        {
            if (client == null)
                return ReturnStatus.InvalidData;

            var duplicatedEmail = await _clientRepository.GetClientByEmail(client.Email);
            if (duplicatedEmail != null)
                return ReturnStatus.DuplicatedEmail;

            client.Active = true;
            _ = await _clientRepository.AddAsync(client);
            return ReturnStatus.Success;
        }

        /// <summary>
        /// Disable Client
        /// </summary>
        public async Task<ReturnStatus> DeleteAsync(int id)
        {
            var client = await _clientRepository.GetClientByIdAsync(id);
            if (client == null)
                return ReturnStatus.EntityDoesNotExist;

            if (_reservationsValidator.HasFutureReservation(client))
                return ReturnStatus.HasFutureReservations;

            client.Active = false;
            _clientRepository.Update(client);
            return ReturnStatus.Success;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Critical Code Smell", "S927:Parameter names should match base declaration and other partial definitions", Justification = "<Pending>")]
        public async Task<ReturnStatus> UpdateAsync(Client client)
        {
            if (client == null)
                return ReturnStatus.EntityDoesNotExist;

            if (_reservationsValidator.HasFutureReservation(client))
            {
                return ReturnStatus.HasFutureReservations;
            }

            _ = await _clientRepository.UpdateAsync(client);
            return ReturnStatus.Success;
        }
    }
}
