﻿using Rental.Data.Enums;
using Rental.Data.Interfaces;
using Rental.Data.Models;
using Rental.Services.BusinessLogic;
using Rental.Services.Enums;
using Rental.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rental.Service.Services
{
    public class RentalService : IRentalService
    {
        private readonly IRentalsRepository _rentalRepository;
        private readonly IVehicleService _vehicleService;
        private readonly IRentalPricesRepository _rentalPricesRepository;
        private readonly IReservationsValidator _reservationsValidator;
        private readonly IClientService _clientService;
        public RentalService(IRentalsRepository rentalRepository,
                            IVehicleService vehicleService,
                            IReservationsValidator reservationsValidator,
                            IRentalPricesRepository rentalPricesRepository,
                            IClientService clientService)
        {
            _rentalRepository = rentalRepository;
            _reservationsValidator = reservationsValidator;
            _vehicleService = vehicleService;
            _rentalPricesRepository = rentalPricesRepository;
            _clientService = clientService;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Critical Code Smell", "S927:Parameter names should match base declaration and other partial definitions", Justification = "<Pending>")]
        public async Task<ReturnStatus> AddAsync(RentalEntry rentalEntry)
        {
            if (rentalEntry == null)
                return ReturnStatus.InvalidData;

            if (rentalEntry.DateFrom <= DateTime.Now || rentalEntry.DateTo <= DateTime.Now)
                return ReturnStatus.InvalidDates;

            var vehicle = await _vehicleService.GetByIdAsync(rentalEntry.VehicleId);
            if (vehicle == null)
                return ReturnStatus.EntityDoesNotExist;

            if (!vehicle.Active)
                return ReturnStatus.DisabledVehicle;

            var client = await _clientService.GetByIdAsync(rentalEntry.ClientId);
            if (client == null)
                return ReturnStatus.EntityDoesNotExist;

            if (!client.Active)
                return ReturnStatus.DisabledClient;

            if (_reservationsValidator.VehicleHasFutureReservationsInRange(rentalEntry))
                return ReturnStatus.HasFutureReservationsInDatesRange;

            rentalEntry.Status = RentalStatus.Confirmed;
            rentalEntry.TotalPrice = GetRentalPrice(rentalEntry);
            _ = await _rentalRepository.AddAsync(rentalEntry);
            return ReturnStatus.Success;
        }

        private double GetRentalPrice(RentalEntry newRental)
        {
            var rentalPrices = _rentalPricesRepository.GetAll().Result.ToArray();

            var frame = newRental.DateTo - newRental.DateFrom;
            if (frame.TotalDays <= 6)
                return (frame.TotalDays * rentalPrices.First(x => x.Frequency == Frequency.Daily).DailyPrice);
            if (frame.TotalDays <= 30)
                return (frame.TotalDays * rentalPrices.First(x => x.Frequency == Frequency.Weekly).DailyPrice);
            if (frame.TotalDays <= 365)
                return (frame.TotalDays * rentalPrices.First(x => x.Frequency == Frequency.Monthly).DailyPrice);
            else
                return (frame.TotalDays * rentalPrices.First(x => x.Frequency == Frequency.Yearly).DailyPrice);
        }

        public async Task<ReturnStatus> CancelRental(int id)
        {
            var rental = await _rentalRepository.Get(id);
            if (rental == null)
                return ReturnStatus.EntityDoesNotExist;

            if (rental.DateFrom <= DateTime.Now && rental.DateTo >= DateTime.Now)
                return ReturnStatus.RentalIsRunning;

            rental.Status = RentalStatus.Cancelled;
            _rentalRepository.Update(rental);
            return ReturnStatus.Success;
        }

        public async Task<RentalEntry> GetByIdAsync(int id)
        {
            return await _rentalRepository.GetRentalByIdAsync(id);
        }
        public async Task<IEnumerable<RentalEntry>> GetAllAsync()
        {
            return await _rentalRepository.GetAllRentalsAsync();
        }

        public Task<ReturnStatus> DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        public Task<ReturnStatus> UpdateAsync(RentalEntry entity)
        {
            throw new NotImplementedException();
        }
    }
}
