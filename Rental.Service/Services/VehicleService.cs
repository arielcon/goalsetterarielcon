﻿using Rental.Data.Interfaces;
using Rental.Data.Models;
using Rental.Services.BusinessLogic;
using Rental.Services.Enums;
using Rental.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rental.Service.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IVehiclesRepository _vehicleRepository;
        private readonly IBrandsRepository _brandsRepository;
        private readonly IReservationsValidator _reservationsValidator;

        public VehicleService(IVehiclesRepository vehicleRepository, IBrandsRepository brandsRepository, IReservationsValidator reservationsValidator)
        {
            _vehicleRepository = vehicleRepository;
            _brandsRepository = brandsRepository;
            _reservationsValidator = reservationsValidator;
        }


        public async Task<IEnumerable<Vehicle>> GetAllAsync()
        {
            return await _vehicleRepository.GetAllVehiclesAsync();
        }

        public async Task<Vehicle> GetByIdAsync(int id)
        {
            return await _vehicleRepository.GetVehicleByIdAsync(id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Critical Code Smell", "S927:Parameter names should match base declaration and other partial definitions", Justification = "<Pending>")]
        public async Task<ReturnStatus> AddAsync(Vehicle vehicle)
        {
            if (vehicle == null)
                return ReturnStatus.InvalidData;

            var brand = await _brandsRepository.Get(vehicle.BrandId);
            if (brand == null)
                return ReturnStatus.RelatedEntityNotFound;

            vehicle.Active = true;
            _ = await _vehicleRepository.AddAsync(vehicle);
            return ReturnStatus.Success;
        }

        /// <summary>
        /// Disable Vehicle
        /// </summary>
        public async Task<ReturnStatus> DeleteAsync(int id)
        {
            var vehicle = await _vehicleRepository.GetVehicleByIdAsync(id);
            if (vehicle == null)
                return ReturnStatus.EntityDoesNotExist;

            if (_reservationsValidator.HasFutureReservation(vehicle))
                return ReturnStatus.HasFutureReservations;

            vehicle.Active = false;
            _vehicleRepository.Update(vehicle);
            return ReturnStatus.Success;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Critical Code Smell", "S927:Parameter names should match base declaration and other partial definitions", Justification = "<Pending>")]
        public async Task<ReturnStatus> UpdateAsync(Vehicle vehicle)
        {
            if (vehicle == null)
                return ReturnStatus.InvalidData;

            var oldVehicle = await _vehicleRepository.GetVehicleByIdAsync(vehicle.Id);
            if (oldVehicle == null)
                return ReturnStatus.EntityDoesNotExist;

            if (_reservationsValidator.HasFutureReservation(vehicle))
            {
                return ReturnStatus.HasFutureReservations;
            }

            _vehicleRepository.Update(vehicle);
            return ReturnStatus.Success;
        }
    }
}
