﻿using Rental.Data.Interfaces;
using Rental.Data.Models;
using Rental.Services.Enums;
using Rental.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rental.Service.Services
{
    public class BrandService : IBrandService
    {
        private readonly IBrandsRepository _brandRepository;
        public BrandService(IBrandsRepository brandRepository)
        {
            _brandRepository = brandRepository;
        }

        public async Task<IEnumerable<Brand>> GetAllAsync()
        {
            return (List<Brand>)await _brandRepository.GetAll();
        }

        public async Task<Brand> GetByIdAsync(int id)
        {
            return await _brandRepository.Get(id);
        }


        public Task<ReturnStatus> AddAsync(Brand entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<ReturnStatus> DeleteAsync(int id)
        {
            throw new System.NotImplementedException();
        }
        public Task<ReturnStatus> UpdateAsync(Brand entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
