﻿using System.ComponentModel;

namespace Rental.Services.Enums
{
    public enum ReturnStatus
    {
        [Description("Invalid Data")]
        InvalidData = 0,

        [Description("Success")]
        Success = 1,

        [Description("Has future reservations")]
        HasFutureReservations = 2,

        [Description("Email already exists")]
        DuplicatedEmail = 3,

        [Description("Unavailable requested dates")]
        InvalidRequestedDates = 4,

        [Description("Can not change the price because there are future reservations")]
        CanNotChangePriceWithFutureReservations = 5,

        [Description("record information not found")]
        EntityDoesNotExist = 6,

        [Description("Has future reservations on the required dates")]
        HasFutureReservationsInDatesRange = 7,

        [Description("Vehicle not available")]
        DisabledVehicle = 8,

        [Description("Client not available")]
        DisabledClient = 9,

        [Description("Related Entity Not Found")]
        RelatedEntityNotFound = 10,

        [Description("Invalid Dates")]
        InvalidDates = 11,

        [Description("Rental is running")]
        RentalIsRunning = 12,

    }
}
