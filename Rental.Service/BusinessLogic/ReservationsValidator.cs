﻿using Rental.Data.Enums;
using Rental.Data.Interfaces;
using Rental.Data.Models;
using System.Linq;

namespace Rental.Services.BusinessLogic
{
    public class ReservationsValidator : IReservationsValidator
    {
        private readonly IRentalsRepository _rentalsRepository;
        public ReservationsValidator(IRentalsRepository rentalsRepository)
        {
            _rentalsRepository = rentalsRepository;
        }
        public bool HasFutureReservation(Client client)
        {
            var exists = _rentalsRepository.GetAll().Result
                .Any(x => x.ClientId == client.Id && x.Status != RentalStatus.Cancelled &&
                     x.DateTo >= System.DateTime.Now);
            return exists;
        }
        public bool HasFutureReservation(Vehicle vehicle)
        {
            var exists = _rentalsRepository.GetAll().Result
                .Any(x => x.VehicleId == vehicle.Id && x.Status != RentalStatus.Cancelled &&
                     x.DateTo >= System.DateTime.Now);
            return exists;
        }

        public bool VehicleHasFutureReservationsInRange(RentalEntry newRental)
        {
            var vehicleRentals = _rentalsRepository.GetAll().Result
                .Where(x => x.VehicleId == newRental.VehicleId && x.Status != RentalStatus.Cancelled);

            var newFrom = newRental.DateFrom;
            var newTo = newRental.DateTo;

            var exists = vehicleRentals.Any(x => newFrom >= x.DateFrom && newFrom <= x.DateTo);
            exists = exists || vehicleRentals.Any(x => newTo >= x.DateFrom && newTo <= x.DateTo);
            exists = exists || vehicleRentals.Any(x => newFrom <= x.DateFrom && newTo >= x.DateTo);

            return exists;
        }
    }
}

