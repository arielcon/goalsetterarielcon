﻿using Rental.Data.Models;

namespace Rental.Services.BusinessLogic
{
    public interface IReservationsValidator
    {
        bool HasFutureReservation(Client client);
        bool HasFutureReservation(Vehicle vehicle);
        bool VehicleHasFutureReservationsInRange(RentalEntry newRental);
    }
}