﻿using Rental.Data.Context;
using Rental.Data.Interfaces;
using Rental.Data.Models;

namespace Rental.Data.Repositories
{
    public class BrandsRepository : GenericRepository<Brand>, IBrandsRepository
    {
        public BrandsRepository(GoalSetterContext context) : base(context)
        {

        }

    }
}
