﻿using Microsoft.EntityFrameworkCore;
using Rental.Data.Context;
using Rental.Data.Interfaces;
using Rental.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rental.Data.Repositories
{
    public class RentalsRepository : GenericRepository<RentalEntry>, IRentalsRepository
    {
        public RentalsRepository(GoalSetterContext context) : base(context)
        {

        }

        public Task<RentalEntry> GetRentalByIdAsync(int id)
        {
            return _context.Rentals.Include(i => i.Vehicle).FirstOrDefaultAsync(p => p.Id == id);

        }
        public async Task<IEnumerable<RentalEntry>> GetAllRentalsAsync()
        {
            return await _context.Rentals.Include(i => i.Vehicle).ToListAsync();
        }

    }
}
