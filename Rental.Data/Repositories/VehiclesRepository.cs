﻿using Microsoft.EntityFrameworkCore;
using Rental.Data.Context;
using Rental.Data.Interfaces;
using Rental.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rental.Data.Repositories
{
    public class VehiclesRepository : GenericRepository<Vehicle>, IVehiclesRepository
    {
        public VehiclesRepository(GoalSetterContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Vehicle>> GetAllVehiclesAsync()
        {
            return await _context.Vehicles.Include(i => i.Brand).ToListAsync();
        }

        public Task<Vehicle> GetVehicleByIdAsync(int id)
        {
            return _context.Vehicles.Include(i => i.Brand).FirstOrDefaultAsync(p => p.Id == id);
        }

        public IEnumerable<Vehicle> GetVehiclesByBrand(string brand)
        {
            return _context.Vehicles.Where(x => x.Brand.Name == brand && x.Active);
        }
    }
}
