﻿using Microsoft.EntityFrameworkCore;
using Rental.Data.Context;
using Rental.Data.Interfaces;
using Rental.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rental.Data.Repositories
{
    public class ClientsRepository : GenericRepository<Client>, IClientsRepository
    {
        public ClientsRepository(GoalSetterContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Client>> GetAllClientsAsync()
        {
            return await _context.Clients.ToListAsync();
        }

        public Task<Client> GetClientByIdAsync(int id)
        {
            return _context.Clients.FirstOrDefaultAsync(x => x.Id == id);
        }

        public IEnumerable<Client> GetClientsByLastName(string lastName)
        {
            return _context.Clients.Where(x => x.LastName == lastName);
        }

        public Task<Client> GetClientByEmail(string email)
        {
            return _context.Clients.FirstOrDefaultAsync(x => x.Email == email);
        }
    }
}
