﻿using Rental.Data.Context;
using Rental.Data.Interfaces;
using Rental.Data.Models;

namespace Rental.Data.Repositories
{
    public class RentalPricesRepository : GenericRepository<RentalPrice>, IRentalPricesRepository
    {
        public RentalPricesRepository(GoalSetterContext context) : base(context)
        {

        }

    }
}
