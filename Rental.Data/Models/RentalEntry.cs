﻿using Rental.Data.Entity;
using Rental.Data.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rental.Data.Models
{
    [Table("Rentals")]
    public class RentalEntry : BaseEntity
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Date From is required")]
        public DateTime DateFrom { get; set; }

        [Required(ErrorMessage = "Date To is required")]
        public DateTime DateTo { get; set; }

        [ForeignKey("Vehicle")]
        public int VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }

        [Required(ErrorMessage = "Total Price is required")]
        public double TotalPrice { get; set; }

        [Required(ErrorMessage = "Status is required")]
        public RentalStatus Status { get; set; }
    }
}
