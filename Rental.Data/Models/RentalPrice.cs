﻿using Rental.Data.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rental.Data.Models
{
    public class RentalPrice
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters")]
        public string PriceDescription { get; set; }

        [Required(ErrorMessage = "Frequency is required")]
        public Frequency Frequency { get; set; }

        [Required(ErrorMessage = "Price is required")]
        public double DailyPrice { get; set; }

        public bool Active { get; set; }
    }
}
