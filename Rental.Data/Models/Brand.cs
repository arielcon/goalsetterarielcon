﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rental.Data.Models
{
    public class Brand
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "Maximum of 50 characters")]
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}
