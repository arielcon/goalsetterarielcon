﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rental.Data.Models
{
    public class Vehicle
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }

        [ForeignKey("Brand")]
        public int BrandId { get; set; }
        public virtual Brand Brand { get; set; }

        public bool Active { get; set; }
    }
}
