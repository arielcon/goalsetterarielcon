﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rental.Data.Entity
{
    public abstract class BaseEntity
    {
        [Column(Order = 1000)]
        public DateTime? CreatedAt { get; set; }
        [Column(Order = 1001)]
        public DateTime? UpdatedAt { get; set; }

    }

}
