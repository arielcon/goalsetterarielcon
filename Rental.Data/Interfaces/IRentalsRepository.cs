﻿using Rental.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rental.Data.Interfaces
{
    public interface IRentalsRepository : IGenericRepository<RentalEntry>
    {
        Task<RentalEntry> GetRentalByIdAsync(int id);
        Task<IEnumerable<RentalEntry>> GetAllRentalsAsync();
    }
}
