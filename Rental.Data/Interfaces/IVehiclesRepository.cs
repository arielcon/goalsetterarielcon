﻿using Rental.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rental.Data.Interfaces
{
    public interface IVehiclesRepository : IGenericRepository<Vehicle>
    {
        IEnumerable<Vehicle> GetVehiclesByBrand(string brand);
        Task<IEnumerable<Vehicle>> GetAllVehiclesAsync();
        Task<Vehicle> GetVehicleByIdAsync(int id);
    }
}
