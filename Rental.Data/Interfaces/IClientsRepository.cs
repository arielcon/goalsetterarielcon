﻿using Rental.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rental.Data.Interfaces
{
    public interface IClientsRepository : IGenericRepository<Client>
    {
        IEnumerable<Client> GetClientsByLastName(string lastName);
        Task<IEnumerable<Client>> GetAllClientsAsync();
        Task<Client> GetClientByIdAsync(int id);
        Task<Client> GetClientByEmail(string email);
    }
}
