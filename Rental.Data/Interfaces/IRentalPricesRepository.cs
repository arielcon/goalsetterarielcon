﻿using Rental.Data.Models;

namespace Rental.Data.Interfaces
{
    public interface IRentalPricesRepository : IGenericRepository<RentalPrice>
    {
    }
}
