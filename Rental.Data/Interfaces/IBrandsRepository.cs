﻿using Rental.Data.Models;

namespace Rental.Data.Interfaces
{
    public interface IBrandsRepository : IGenericRepository<Brand>
    {
    }
}
