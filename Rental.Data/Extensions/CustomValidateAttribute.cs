﻿using System.ComponentModel.DataAnnotations;

namespace Rental.Data.Extensions
{
    public class CustomValidateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString().Trim()))
            {
                return new ValidationResult(validationContext.DisplayName + " is required.");
            }

            return ValidationResult.Success;
        }
    }

}
