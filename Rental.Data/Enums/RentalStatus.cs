﻿namespace Rental.Data.Enums
{
    public enum RentalStatus
    {
        Cancelled = 0,
        Requested = 1,
        Confirmed = 2
    }
}
