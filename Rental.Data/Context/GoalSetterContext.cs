﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Rental.Data.Entity;
using Rental.Data.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Rental.Data.Context
{
    public class GoalSetterContext : DbContext
    {
        protected string ConnectionString { get; set; }
        public IConfiguration _configuration { get; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<RentalEntry> Rentals { get; set; }
        public DbSet<RentalPrice> RentalPrices { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        public GoalSetterContext()
        {

        }
        public GoalSetterContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public GoalSetterContext(string connectionString) : base(GetOptions(connectionString))
        {
            this.ConnectionString = connectionString;
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Method intentionally left empty.
        }

        private static DbContextOptions GetOptions(string connectionString)
        {
            return SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionString).Options;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string connectionString = _configuration.GetConnectionString("DefaultConnection");
                if (connectionString != null)
                {
                    optionsBuilder.UseSqlServer(connectionString);
                }
                else
                {
                    optionsBuilder.UseInMemoryDatabase(databaseName: "goalsetter_in_memory");
                }
                optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            AddTimestamps();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
            {
                var now = DateTime.UtcNow; // current datetime
                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity)entity.Entity).CreatedAt = now;
                }
                ((BaseEntity)entity.Entity).UpdatedAt = now;
            }
        }

    }
}
