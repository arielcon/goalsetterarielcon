## Goalsetter .Net Test ##

**Ariel Con -  ariel.con@gmail.com**


### Requirements: ###
- Visual Studio 2019, SQL Server 2016 or higher

### Testing notes: ###
- Set up the appsettings.json file to set Database connection parameters
- Database is deleted and created automatically on each start
- Initialization data is filled automatically (check appsettings.json parameters)
- I decided to use the same simple test data for the Application and the Test project too, instead of using a 3rd party Mock package. There is a simple Test data initialization project/class.
- To test the 3 REST API controllers, **there are samples in TestPostman folder**
- For using Postman, body parameters must be filled with the RAW option and Content-Type = application/json
- IISExpress launches the app at http://localhost:60427
- **check TestPostman folder**

### Assumptions: ###
- Each delete action on Clients, Rentals and Vehicles, does a CANCEL operation (logic delete) because rentals history information must be kept
- custom API routes were not required, I left the standard/default routes
- Web APIs security is not required



