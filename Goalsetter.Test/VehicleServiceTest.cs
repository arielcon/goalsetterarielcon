﻿using Rental.Data.Models;
using Rental.Services.Enums;
using Rental.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Goalsetter.Tests
{
    public class VehicleServiceTest : GoalSetterTestConfig
    {
        private readonly IVehicleService _vehicleService;
        public VehicleServiceTest()
        {
            _vehicleService = base.Get<IVehicleService>();
        }

        [Fact]
        public void Can_Add_Vehicle()
        {
            var idBrand = _allBrands.First().Id;
            Assert.Equal(ReturnStatus.Success, _vehicleService.AddAsync(new Vehicle() { BrandId = idBrand, Active = true }).Result);
        }

        [Fact]
        public void Cant_Add_Vehicle()
        {
            Assert.Equal(ReturnStatus.InvalidData, _vehicleService.AddAsync(null).Result);
            _ = Assert.ThrowsAsync<Exception>(() => _vehicleService.AddAsync(new Vehicle() { BrandId = -1, Active = true }));

            var idBrand = _allBrands.First().Id;
            _ = Assert.ThrowsAsync<Exception>(() => _vehicleService.AddAsync(
                new Vehicle() { BrandId = idBrand, Active = true }));
        }

        [Fact]
        public void Cant_Disable_Vehicle()
        {
            var idWithRental = _allRentals.First().VehicleId;
            var idNotExists = _allVehicles.Max(x => x.Id) + 1;

            Assert.Equal(ReturnStatus.EntityDoesNotExist, _vehicleService.DeleteAsync(idNotExists).Result);
            Assert.Equal(ReturnStatus.HasFutureReservations, _vehicleService.DeleteAsync(idWithRental).Result);
        }

        [Fact]
        public void Can_Disable_Vehicle()
        {
            var vehiclesWithRentals = _allRentals.Select(x => x.VehicleId).Distinct();
            var idWithoutRental = _allVehicles.First(x => !vehiclesWithRentals.Contains(x.Id)).Id;

            Assert.Equal(ReturnStatus.Success, _vehicleService.DeleteAsync(idWithoutRental).Result);
        }

        [Fact]
        public async Task Can_Get_All_Vehicles()
        {
            var vehicles = await _vehicleService.GetAllAsync();
            Assert.Equal(5, vehicles.Count());
            Assert.Contains(vehicles, p => p.Brand.Name == "BMW");
        }

        [Fact]
        public void Can_Get_One_Vehicle()
        {
            var vehicle = _allVehicles.Last();
            Assert.Equal(vehicle.Id, _vehicleService.GetByIdAsync(vehicle.Id).Result.Id);
        }
    }
}
