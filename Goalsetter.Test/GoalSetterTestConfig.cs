﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Rental.Data.Context;
using Rental.Data.Interfaces;
using Rental.Data.Models;
using Rental.Data.Repositories;
using Rental.Service.Services;
using Rental.Services.BusinessLogic;
using Rental.Services.Interfaces;
using Rental.TestData;
using System.Collections.Generic;
using Xunit;

[assembly: CollectionBehavior(MaxParallelThreads = 1)]

namespace Goalsetter.Tests
{
    public class GoalSetterTestConfig
    {
        private readonly IServiceCollection _services;
        public GoalSetterTestConfig()
        {
            _services = new ServiceCollection();
            _services.AddDbContext<GoalSetterContext>(options =>
                                 options.UseInMemoryDatabase("goalsetter_in_memory"),
                                 ServiceLifetime.Transient);

            _services.AddTransient<IVehiclesRepository, VehiclesRepository>();
            _services.AddTransient<IBrandsRepository, BrandsRepository>();
            _services.AddTransient<IRentalsRepository, RentalsRepository>();
            _services.AddTransient<IClientsRepository, ClientsRepository>();
            _services.AddTransient<IRentalPricesRepository, RentalPricesRepository>();

            _services.AddTransient<IClientService, ClientService>();
            _services.AddTransient<IVehicleService, VehicleService>();
            _services.AddTransient<IBrandService, BrandService>();
            _services.AddTransient<IRentalService, RentalService>();
            _services.AddTransient<IReservationsValidator, ReservationsValidator>();

            var provider = _services.BuildServiceProvider();
            DbInitializer.InitializeWithData(provider); // Insert default data
        }

        public T Get<T>()
        {
            var provider = _services.BuildServiceProvider();
            var restService = provider.GetRequiredService<T>();

            return restService;
        }

        #region TESTING_DATA_COLLECTIONS
        protected IEnumerable<Brand> _allBrands
        {
            get
            {
                var provider = _services.BuildServiceProvider();
                return provider.GetRequiredService<GoalSetterContext>().Brands.ToListAsync().Result;
            }
        }
        protected IEnumerable<Client> _allClients
        {
            get
            {
                var provider = _services.BuildServiceProvider();
                return provider.GetRequiredService<GoalSetterContext>().Clients.ToListAsync().Result;
            }
        }
        protected IEnumerable<RentalEntry> _allRentals
        {
            get
            {
                var provider = _services.BuildServiceProvider();
                return provider.GetRequiredService<GoalSetterContext>().Rentals.ToListAsync().Result;
            }
        }
        protected IEnumerable<Vehicle> _allVehicles
        {
            get
            {
                var provider = _services.BuildServiceProvider();
                return provider.GetRequiredService<GoalSetterContext>().Vehicles.ToListAsync().Result;
            }
        }
        #endregion

    }
}
