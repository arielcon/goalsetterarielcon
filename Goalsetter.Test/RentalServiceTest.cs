﻿using Rental.Data.Enums;
using Rental.Data.Models;
using Rental.Services.Enums;
using Rental.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Goalsetter.Tests
{
    public class RentalServiceTest : GoalSetterTestConfig
    {
        private readonly IRentalService _rentalService;
        public RentalServiceTest()
        {
            _rentalService = base.Get<IRentalService>();
        }


        [Fact]
        public void Can_Add_Rental()
        {
            var idClient = _allClients.First(x => x.Active).Id;
            var idVehicle = _allVehicles.First(x => x.Active).Id;
            var newRental = new RentalEntry()
            {
                ClientId = idClient,
                VehicleId = idVehicle,
                DateFrom = DateTime.Now.AddDays(+100),
                DateTo = DateTime.Now.AddDays(102),
                Status = RentalStatus.Confirmed
            };
            Assert.Equal(ReturnStatus.Success, _rentalService.AddAsync(newRental).Result);
        }

        [Fact]
        public void Cant_Add_Rental()
        {
            var someFutureRental = _allRentals.First(x => x.DateFrom >= DateTime.Now && x.Status == RentalStatus.Confirmed && ((x.DateTo - x.DateFrom).Days > 1));
            var clientIdOk = _allClients.First(x => x.Active).Id;
            var vehicleIdOk = _allVehicles.First(x => x.Active).Id;
            var vehicleIdDisabled = _allVehicles.First(x => !x.Active).Id;
            var clientIdDisabled = _allClients.First(x => !x.Active).Id;

            var newRental = new RentalEntry()
            {
                ClientId = clientIdOk,
                VehicleId = vehicleIdOk,
                DateFrom = DateTime.Now.AddDays(+100),
                DateTo = DateTime.Now.AddDays(102),
                Status = RentalStatus.Confirmed
            };

            Assert.Equal(ReturnStatus.InvalidData, _rentalService.AddAsync(null).Result);

            newRental.ClientId = clientIdDisabled;
            Assert.Equal(ReturnStatus.DisabledClient, _rentalService.AddAsync(newRental).Result);

            newRental.ClientId = clientIdOk;
            newRental.VehicleId = vehicleIdDisabled;
            Assert.Equal(ReturnStatus.DisabledVehicle, _rentalService.AddAsync(newRental).Result);

            newRental.VehicleId = vehicleIdOk;
            newRental.DateFrom = DateTime.Now.AddDays(-1);
            Assert.Equal(ReturnStatus.InvalidDates, _rentalService.AddAsync(newRental).Result);

            newRental.DateFrom = DateTime.Now.AddDays(1);
            newRental.DateTo = DateTime.Now.AddDays(-1);
            Assert.Equal(ReturnStatus.InvalidDates, _rentalService.AddAsync(newRental).Result);

            newRental.DateFrom = someFutureRental.DateFrom;
            newRental.DateTo = someFutureRental.DateTo.AddDays(1);
            newRental.VehicleId = someFutureRental.VehicleId;
            newRental.DateFrom = someFutureRental.DateFrom;
            Assert.Equal(ReturnStatus.HasFutureReservationsInDatesRange, _rentalService.AddAsync(newRental).Result);

            newRental.DateFrom = someFutureRental.DateFrom.AddDays(1);
            newRental.DateTo = someFutureRental.DateTo;
            Assert.Equal(ReturnStatus.HasFutureReservationsInDatesRange, _rentalService.AddAsync(newRental).Result);

            newRental.DateFrom = someFutureRental.DateFrom.AddDays(1);
            newRental.DateTo = someFutureRental.DateTo.AddDays(-1);
            Assert.Equal(ReturnStatus.HasFutureReservationsInDatesRange, _rentalService.AddAsync(newRental).Result);

        }

        [Fact]
        public void Cant_Disable_Rental()
        {
            var idNotExists = _allRentals.Max(x => x.Id) + 1;
            Assert.Equal(ReturnStatus.EntityDoesNotExist, _rentalService.CancelRental(idNotExists).Result);

            var idRunningRental = _allRentals.First(x => x.DateFrom <= DateTime.Now && x.DateTo >= DateTime.Now && x.Status == RentalStatus.Confirmed).Id;
            Assert.Equal(ReturnStatus.RentalIsRunning, _rentalService.CancelRental(idRunningRental).Result);
        }

        [Fact]
        public void Can_Disable_Rental()
        {
            var idRental = _allRentals.First().Id;
            Assert.Equal(ReturnStatus.Success, _rentalService.CancelRental(idRental).Result);
        }

        [Fact]
        public async Task Can_Get_All_Rentals()
        {
            var rentals = await _rentalService.GetAllAsync();
            Assert.Equal(3, rentals.Count());
            Assert.Contains(rentals, p => p.TotalPrice == 555 && p.VehicleId == 1);
        }

        [Fact]
        public void Can_Get_One_Rental()
        {
            var rentalId = _allRentals.Last().Id;
            Assert.Equal(rentalId, _rentalService.GetByIdAsync(rentalId).Result.Id);
        }
    }
}
