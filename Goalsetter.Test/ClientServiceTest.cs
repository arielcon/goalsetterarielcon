﻿using Rental.Data.Models;
using Rental.Services.Enums;
using Rental.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Goalsetter.Tests
{
    public class ClientServiceTest : GoalSetterTestConfig
    {
        private readonly IClientService _clientService;
        public ClientServiceTest()
        {
            _clientService = base.Get<IClientService>();
        }

        [Fact]
        public void Can_Add_Client()
        {
            var newClient = new Client() { Active = true, Email = "somenewemail@domain.com", Address = "some street", FirstName = "ArielTest", LastName = "Test", Country = "Arg" };
            Assert.Equal(ReturnStatus.Success, _clientService.AddAsync(newClient).Result);
        }

        [Fact]
        public void Cant_Add_Client()
        {
            var existingEmail = _allClients.First().Email;
            var newClient = new Client() { Active = true, Email = existingEmail, Address = "some street", FirstName = "ArielTest", LastName = "Test", Country = "Arg" };
            Assert.Equal(ReturnStatus.DuplicatedEmail, _clientService.AddAsync(newClient).Result);

            var badClient = new Client() { Active = true, Email = "somenewemail22@domain.com", Address = "some street", FirstName = "", LastName = "", Country = "Arg" };
            _ = Assert.ThrowsAsync<Exception>(() => _clientService.AddAsync(badClient));

        }

        [Fact]
        public async Task Can_Get_All_Clients()
        {
            var clients = await _clientService.GetAllAsync();
            Assert.Equal(4, clients.Count());
            Assert.Contains(clients, p => p.FirstName == "Ariel 2");
        }

        [Fact]
        public void Cant_Disable_Client()
        {
            var idWithRental = _allRentals.First().ClientId;
            var idNotExists = _allClients.Max(x => x.Id) + 1;

            Assert.Equal(ReturnStatus.EntityDoesNotExist, _clientService.DeleteAsync(idNotExists).Result);
            Assert.Equal(ReturnStatus.HasFutureReservations, _clientService.DeleteAsync(idWithRental).Result);
        }

        [Fact]
        public void Can_Disable_Client()
        {
            var clientsWithRentals = _allRentals.Select(x => x.ClientId).Distinct();
            var idWithoutRental = _allClients.First(x => !clientsWithRentals.Contains(x.Id)).Id;

            Assert.Equal(ReturnStatus.Success, _clientService.DeleteAsync(idWithoutRental).Result);
        }

        [Fact]
        public void Can_Get_One_Client()
        {
            var client = _allClients.Last();
            Assert.Equal(client.Id, _clientService.GetByIdAsync(client.Id).Result.Id);
        }

    }
}
