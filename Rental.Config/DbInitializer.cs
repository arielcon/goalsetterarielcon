﻿using Microsoft.Extensions.DependencyInjection;
using Rental.Data.Context;
using Rental.Data.Enums;
using Rental.Data.Models;
using System;

namespace Rental.TestData
{
    public static class DbInitializer
    {
        public static void InitializeWithData(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<GoalSetterContext>();
            context.Database.EnsureDeleted();
            if (context.Database.EnsureCreated())
            {
                context.Brands.Add(new Brand { Name = "Audi", Active = true });
                context.Brands.Add(new Brand { Name = "BMW", Active = true });
                context.Brands.Add(new Brand { Name = "Cytroen", Active = false });
                context.SaveChanges();

                context.RentalPrices.Add(new RentalPrice() { Frequency = Frequency.Daily, DailyPrice = 200, Active = true, PriceDescription = "Precio diario" });
                context.RentalPrices.Add(new RentalPrice() { Frequency = Frequency.Weekly, DailyPrice = 150, Active = true, PriceDescription = "Promoción semanal" });
                context.RentalPrices.Add(new RentalPrice() { Frequency = Frequency.Monthly, DailyPrice = 120, Active = true, PriceDescription = "Promoción mensual" });
                context.RentalPrices.Add(new RentalPrice() { Frequency = Frequency.Yearly, DailyPrice = 100, Active = true, PriceDescription = "Promoción anual" });
                context.SaveChanges();

                context.Vehicles.Add(new Vehicle { BrandId = 1, Active = true });
                context.Vehicles.Add(new Vehicle { BrandId = 2, Active = true });
                context.Vehicles.Add(new Vehicle { BrandId = 3, Active = true });
                context.Vehicles.Add(new Vehicle { BrandId = 1, Active = false });
                context.Vehicles.Add(new Vehicle { BrandId = 2, Active = false });
                context.SaveChanges();

                var client = new Client() { Address = "Camargo", City = "Buenos Aires", Country = "Argentina", FirstName = "Ariel", LastName = "Con", State = "CABA", Email = "ariel@gmail.com", Active = true };
                var client2 = new Client() { Address = "Pseo Victorica", City = "Tigre", Country = "Argentina", FirstName = "Ariel 2", LastName = "Con", State = "Buenos Aires", Email = "ariel2@gmail.com", Active = true };
                var client3 = new Client() { Address = "Corrientes 4479", City = "Bs.As.", Country = "Argentina", FirstName = "Ariel sin rentals", LastName = "Ariel ...", State = "CABA", Email = "ariel33@gmail.com", Active = true };
                var client4 = new Client() { Address = "---------", City = "Bs.As.", Country = "Argentina", FirstName = "disabled client", LastName = "disabled client", State = "CABA", Email = "ariel33@gmail.com", Active = false };
                context.Clients.AddRange(client, client2, client3, client4);
                context.SaveChanges();

                var rental1 = new RentalEntry() { DateFrom = DateTime.Now, DateTo = DateTime.Now.AddDays(5), ClientId = 2, Status = RentalStatus.Confirmed, TotalPrice = 333, VehicleId = 1 };
                var rental2 = new RentalEntry() { DateFrom = DateTime.Now.AddDays(10), DateTo = DateTime.Now.AddDays(20), ClientId = 1, Status = RentalStatus.Confirmed, TotalPrice = 333, VehicleId = 2 };
                var rental3 = new RentalEntry() { DateFrom = DateTime.Now.AddDays(30), DateTo = DateTime.Now.AddDays(40), ClientId = 2, Status = RentalStatus.Requested, TotalPrice = 555, VehicleId = 1 };
                context.Rentals.AddRange(rental1, rental2, rental3);
                context.SaveChanges();
            }
        }
    }
}
