﻿using System.ComponentModel.DataAnnotations;

namespace Rental.Presentation.DTOs
{
    public class ClientDto
    {
        public int Id { get; set; }

        [Required, StringLength(50, ErrorMessage = "Maximum of 50 characters")]
        public string FirstName { get; set; }

        [Required, StringLength(50, ErrorMessage = "Maximum of 50 characters")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Required, StringLength(200, ErrorMessage = "Maximum of 200 characters")]
        public string Address { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters")]
        public string City { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters")]
        public string State { get; set; }

        [Required, StringLength(50, ErrorMessage = "Maximum of 50 characters")]
        public string Country { get; set; }

        public bool Active { get; set; }
    }
}
