﻿using System.ComponentModel.DataAnnotations;

namespace Rental.Presentation.DTOs
{
    public class VehicleDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "BrandId is required.")]
        public int? BrandId { get; set; }
        public bool Active { get; set; }
    }
}
