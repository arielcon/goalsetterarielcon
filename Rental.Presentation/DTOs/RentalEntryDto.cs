﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Rental.Presentation.DTOs
{
    public class RentalEntryDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Date From is required")]
        public DateTime DateFrom { get; set; }

        [Required(ErrorMessage = "Date To is required")]
        public DateTime DateTo { get; set; }

        [Required]
        public int? VehicleId { get; set; }

        [Required]
        public int? ClientId { get; set; }

        [Required(ErrorMessage = "Status is required")]
        public int? RentalStatus { get; set; }
    }
}
