﻿using System.ComponentModel.DataAnnotations;

namespace Rental.Presentation.DTOs
{
    public class RentalPriceDto
    {
        public int Id { get; set; }

        [Required, StringLength(50, ErrorMessage = "Maximum of 50 characters")]
        public string PriceDescription { get; set; }

        [Required(ErrorMessage = "Frequency is required")]
        public int Frequency { get; set; }

        [Required(ErrorMessage = "Price is required")]
        public double DailyPrice { get; set; }
    }
}
