﻿using System.ComponentModel.DataAnnotations;

namespace Rental.Presentation.DTOs
{
    public class BrandDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "Maximum of 50 characters")]
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}
